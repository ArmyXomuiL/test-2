//
//  AppDelegate.swift
//  LineChart
//
//  Created by Дмитрий Пономарев on 08.04.2018.
//  Copyright © 2018 AXL. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  var coordinator = MainCoordinator()

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    prepareWindow()
    coordinator.startCoordinatorWithWindow(window!)
    
    return true
  }

  func applicationWillResignActive(_ application: UIApplication) { }

  func applicationDidEnterBackground(_ application: UIApplication) { }

  func applicationWillEnterForeground(_ application: UIApplication) { }

  func applicationDidBecomeActive(_ application: UIApplication) { }

  func applicationWillTerminate(_ application: UIApplication) { }
  
  //MARK: - Private
  
  fileprivate func prepareWindow() {
    window = UIWindow(frame: UIScreen.main.bounds)
    window!.backgroundColor = UIColor.white
    window!.makeKeyAndVisible()
  }
}
