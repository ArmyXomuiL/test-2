//
//  BondQuotationManager.swift
//  LineChart
//
//  Created by Дмитрий Пономарев on 08.04.2018.
//  Copyright © 2018 AXL. All rights reserved.
//

import UIKit

class BondQuotationManager: NSObject {
  class func loadQuotation(of bond: String, with timeframe: Timeframe, and chartType: TypeOfChart, with success: @escaping SuccessWithArrayWithDouble, and failure: @escaping FailureWithError) {
    //Simulate API
    
    DispatchQueue.global().async {
      //Wait backend
      sleep(1)
      
      if bond == "ISIN" {
        //Take API success
        let quotations = data(for: timeframe, and: chartType)
        
        DispatchQueue.main.async {
          success(quotations)
        }
      } else {
        //Take API error
        let error = NSError(domain: "Incorrect identifier", code: 500, userInfo: nil)
        
        DispatchQueue.main.async {
          failure(error)
        }
      }
    }
  }
  
  //MARK: - Simulate API
  
  private class func data(for timeframe: Timeframe, and chartType: TypeOfChart) -> [Double] {
    let range = chartType == .yieled ? 50 : 300
    
    switch timeframe {
    case .oneWeek:      return randomNumbers(with: 7, and: range)
    case .oneMonth:     return randomNumbers(with: 30, and: range)
    case .threeMonths:  return randomNumbers(with: 30, and: range)
    case .sixMonths:    return randomNumbers(with: 30, and: range)
    case .oneYear:      return randomNumbers(with: 30, and: range)
    case .twoYears:     return randomNumbers(with: 30, and: range)
    }
  }
  
  private class func randomNumbers(with count: Int, and range: Int) -> [Double] {
    var numbers = [Double]()
    
    for _ in 0..<count {
      numbers += [Double(arc4random_uniform(UInt32(range)))]
    }
    
    return numbers
  }
}
