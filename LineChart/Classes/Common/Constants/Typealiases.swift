//
//  Typealiases.swift
//  LineChart
//
//  Created by Дмитрий Пономарев on 08.04.2018.
//  Copyright © 2018 AXL. All rights reserved.
//

import UIKit

typealias SuccessWithArrayWithDouble  = ([Double]) -> Void
typealias FailureWithError            = (NSError) -> Void
