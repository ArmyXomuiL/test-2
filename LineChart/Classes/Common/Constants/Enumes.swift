//
//  Enumes.swift
//  LineChart
//
//  Created by Дмитрий Пономарев on 08.04.2018.
//  Copyright © 2018 AXL. All rights reserved.
//

import UIKit

enum Timeframe: String {
  case oneWeek      = "1W"
  case oneMonth     = "1M"
  case threeMonths  = "3M"
  case sixMonths    = "6M"
  case oneYear      = "1Y"
  case twoYears     = "2Y"
}

enum TypeOfChart: String {
  case yieled = "Yieled"
  case price = "Price"
}
