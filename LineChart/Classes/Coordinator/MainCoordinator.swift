//
//  MainCoordinator.swift
//  LineChart
//
//  Created by Дмитрий Пономарев on 08.04.2018.
//  Copyright © 2018 AXL. All rights reserved.
//

import UIKit

class MainCoordinator: NSObject {
  var window: UIWindow!
  
  lazy var chart: ChartHeader = {
    return ChartHeader(root: self)
  }()
  
  func startCoordinatorWithWindow(_ window: UIWindow) {
    self.window = window
    window.rootViewController = chart.currentController()
  }
}

extension MainCoordinator: ChartDelegate {
  
}
