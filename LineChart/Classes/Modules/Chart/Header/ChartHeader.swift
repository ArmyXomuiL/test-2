//
//  ChartChartHeader.swift
//  LineChart
//
//  Created by ArmyXomuiL on 08/04/2018.
//  Copyright 2018 AXL. All rights reserved.
//

import UIKit

class ChartHeader: NSObject {

  var controller: ChartViewController!
  var presenter: ChartPresenter!
  var interactor: ChartInteractor!

  init(root: ChartDelegate) {
    super.init()

    controller = moduleController()
    presenter = ChartPresenter()
    interactor = ChartInteractor()

    presenter.root = root
    presenter.controller = controller
    presenter.interactor = interactor

    controller.presenter = presenter
    interactor.presenter = presenter
  }

  func currentController() -> UIViewController {
    return controller
  }

  //MARK: - private

  private func moduleController() -> ChartViewController {
    return moduleStoryboard().instantiateViewController(withIdentifier: "ChartViewController") as! ChartViewController
  }

  private func moduleStoryboard() -> UIStoryboard {
    return UIStoryboard(name: "Chart", bundle: nil)
  }
}

extension ChartHeader: ChartInterface {

}
