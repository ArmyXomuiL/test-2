//
//  ChartViewController+ChartControllerInterface.swift
//  LineChart
//
//  Created by Дмитрий Пономарев on 09.04.2018.
//  Copyright © 2018 AXL. All rights reserved.
//

import UIKit
import MBProgressHUD
import Charts

extension ChartViewController: ChartControllerInterface {
  func updateView(with data: ChartData) {
    chartView.data = data
  }
  
  func show(errorMessage: String) {
    let alert = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "ОК", style: UIAlertAction.Style.default, handler: nil))
    self.present(alert, animated: true, completion: nil)
  }
  
  func showActivityIndicator() {
    MBProgressHUD.showAdded(to: view, animated: true)
  }
  
  func hideActivityIndicator() {
    MBProgressHUD.hide(for: view, animated: true)
  }
}
