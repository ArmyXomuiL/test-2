//
//  ChartViewController+BSKeyboardControlsDelegate.swift
//  LineChart
//
//  Created by Дмитрий Пономарев on 09.04.2018.
//  Copyright © 2018 AXL. All rights reserved.
//

import UIKit
import BSKeyboardControls

extension ChartViewController: BSKeyboardControlsDelegate {
  func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls!) {
    if let activeField = keyboardControls.activeField {
      activeField.resignFirstResponder()
      
      let chartType: TypeOfChart = typeOfChart.selectedSegmentIndex == 0 ? .yieled : .price
      presenter?.didChangeIdentificator(with: formatter.timeframe, with: identificatorTextField.text!, and: chartType)
    }
  }
}
