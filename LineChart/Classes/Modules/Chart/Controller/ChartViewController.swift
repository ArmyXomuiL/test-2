//
//  ChartChartViewController.swift
//  LineChart
//
//  Created by ArmyXomuiL on 08/04/2018.
//  Copyright 2018 AXL. All rights reserved.
//

import UIKit
import Charts
import BSKeyboardControls

class ChartViewController: UIViewController {
  weak var presenter: ChartControllerDelegate?

  @IBOutlet weak var chartView: LineChartView!
  @IBOutlet weak var selectedTimeframe: UIButton!
  @IBOutlet weak var identificatorTextField: UITextField!
  @IBOutlet weak var typeOfChart: UISegmentedControl!
  
  var formatter: DayAxisValueFormatter!
  var keyboardControls = BSKeyboardControls()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    prepareKeyboard()
    prepareChartView()
    
    presenter?.didReadyToWork()
  }
  
  //MARK: - Actions
  
  @IBAction func oneWeekButtonAction(_ sender: UIButton) {
    updateSelectedOfTimeframeButtons(with: sender)
    formatter.timeframe = .oneWeek
    sendToPresenterCurrentState()
  }
  
  @IBAction func oneMonthButtonAction(_ sender: UIButton) {
    updateSelectedOfTimeframeButtons(with: sender)
    formatter.timeframe = .oneMonth
    sendToPresenterCurrentState()
  }
  
  @IBAction func threeMonthsButtonAction(_ sender: UIButton) {
    updateSelectedOfTimeframeButtons(with: sender)
    formatter.timeframe = .threeMonths
    sendToPresenterCurrentState()
  }
  
  @IBAction func sixMonthsButtonAction(_ sender: UIButton) {
    updateSelectedOfTimeframeButtons(with: sender)
    formatter.timeframe = .sixMonths
    sendToPresenterCurrentState()
  }
  
  @IBAction func oneYearButtonAction(_ sender: UIButton) {
    updateSelectedOfTimeframeButtons(with: sender)
    formatter.timeframe = .oneYear
    sendToPresenterCurrentState()
  }
  
  @IBAction func twoYearsButtonAction(_ sender: UIButton) {
    updateSelectedOfTimeframeButtons(with: sender)
    formatter.timeframe = .twoYears
    sendToPresenterCurrentState()
  }
  
  @IBAction func typeOfChartAction(_ sender: UISegmentedControl) {
    presenter?.didChangeChartType(with: formatter.timeframe, with: identificatorTextField.text!, and: chartType())
  }
  
  //MARK: - Private
  
  private func sendToPresenterCurrentState() {
    presenter?.didPressTimeframeButton(with: formatter.timeframe, with: identificatorTextField.text!, and: chartType())
  }
  
  private func prepareKeyboard() {
    let fields = [identificatorTextField] as [Any]
    keyboardControls = BSKeyboardControls(fields: fields)
    keyboardControls.delegate = self
  }
  
  private func updateSelectedOfTimeframeButtons(with sender: UIButton) {
    selectedTimeframe.isSelected = false
    selectedTimeframe = sender
    sender.isSelected = true
  }
  
  private func prepareChartView() {
    chartView.chartDescription?.enabled = false
    chartView.setScaleEnabled(true)
    chartView.pinchZoomEnabled = true
    
    formatter = DayAxisValueFormatter(chart: chartView)
    
    let xAxis = chartView.xAxis
    xAxis.labelPosition = .bottom
    xAxis.labelFont = .systemFont(ofSize: 10)
    xAxis.granularity = 1
    xAxis.labelCount = 10
    xAxis.valueFormatter = formatter
    
    chartView.rightAxis.enabled = false
  }
  
  private func chartType() -> TypeOfChart {
    return typeOfChart.selectedSegmentIndex == 0 ? .yieled : .price
  }
}
