//
//  ChartViewController+UITextFieldDelegate.swift
//  LineChart
//
//  Created by Дмитрий Пономарев on 09.04.2018.
//  Copyright © 2018 AXL. All rights reserved.
//

import UIKit

extension ChartViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    keyboardControls.activeField.resignFirstResponder()
    
    let chartType: TypeOfChart = typeOfChart.selectedSegmentIndex == 0 ? .yieled : .price
    presenter?.didChangeIdentificator(with: formatter.timeframe, with: identificatorTextField.text!, and: chartType)
    
    return true
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    keyboardControls.activeField = textField
  }
}
