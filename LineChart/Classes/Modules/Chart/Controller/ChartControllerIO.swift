//
//  ChartChartControllerIO.swift
//  LineChart
//
//  Created by ArmyXomuiL on 08/04/2018.
//  Copyright 2018 AXL. All rights reserved.
//

import Foundation
import Charts

protocol ChartControllerInterface: class {
  func updateView(with data: ChartData)
  func show(errorMessage: String)
  
  func showActivityIndicator()
  func hideActivityIndicator()
}

protocol ChartControllerDelegate: class {
  func didReadyToWork()
  
  func didPressTimeframeButton(with type: Timeframe, with identificator: String, and chartType: TypeOfChart)
  func didChangeIdentificator(with type: Timeframe, with identificator: String, and tychartTypepe: TypeOfChart)
  func didChangeChartType(with type: Timeframe, with identificator: String, and chartType: TypeOfChart)
}
