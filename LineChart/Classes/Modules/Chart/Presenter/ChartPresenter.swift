//
//  ChartChartPresenter.swift
//  LineChart
//
//  Created by ArmyXomuiL on 08/04/2018.
//  Copyright 2018 AXL. All rights reserved.
//

import Foundation
import Charts

class ChartPresenter: NSObject {
  weak var root: ChartDelegate?
  weak var controller: ChartControllerInterface?
  weak var interactor: ChartInteractorInterface?
}

extension ChartPresenter: ChartInterface {

}

extension ChartPresenter: ChartControllerDelegate {
  func didReadyToWork() {
    controller?.showActivityIndicator()
    interactor?.loadData(for: .oneWeek, for: "ISIN", and: .yieled)
  }
  
  func didPressTimeframeButton(with type: Timeframe, with identificator: String, and chartType: TypeOfChart) {
    controller?.showActivityIndicator()
    interactor?.loadData(for: type, for: identificator, and: chartType)
  }
  
  func didChangeIdentificator(with type: Timeframe, with identificator: String, and chartType: TypeOfChart) {
    controller?.showActivityIndicator()
    interactor?.loadData(for: type, for: identificator, and: chartType)
  }
  
  func didChangeChartType(with type: Timeframe, with identificator: String, and chartType: TypeOfChart) {
    controller?.showActivityIndicator()
    interactor?.loadData(for: type, for: identificator, and: chartType)
  }
}

extension ChartPresenter: ChartInteractorDelegate {
  func didLoad(data: ChartData) {
    controller?.hideActivityIndicator()
    controller?.updateView(with: data)
  }
  
  func didFailLoadData(with error: NSError) {
    controller?.hideActivityIndicator()
    controller?.updateView(with: ChartData(dataSets: nil))
    controller?.show(errorMessage: error.domain)
  }
}
