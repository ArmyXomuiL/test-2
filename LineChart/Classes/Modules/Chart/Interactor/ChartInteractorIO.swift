//
//  ChartChartInteractorIO.swift
//  LineChart
//
//  Created by ArmyXomuiL on 08/04/2018.
//  Copyright 2018 AXL. All rights reserved.
//

import Foundation
import Charts

protocol ChartInteractorInterface: class {
  func loadData(for timeframe: Timeframe, for identificator: String, and chartType: TypeOfChart)
}

protocol ChartInteractorDelegate: class {
  func didLoad(data: ChartData)
  func didFailLoadData(with error: NSError)
}
