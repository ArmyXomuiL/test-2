//
//  ChartChartInteractor.swift
//  LineChart
//
//  Created by ArmyXomuiL on 08/04/2018.
//  Copyright 2018 AXL. All rights reserved.
//

import Foundation
import Charts

class ChartInteractor: NSObject {
  weak var presenter: ChartInteractorDelegate?
}

extension ChartInteractor: ChartInteractorInterface {
  func loadData(for timeframe: Timeframe, for identificator: String, and chartType: TypeOfChart) {
    BondQuotationManager.loadQuotation(of: identificator, with: timeframe, and: chartType, with: { (quotations) in
      let data = self.prepareData(from: quotations, with: identificator)
      self.presenter?.didLoad(data: data)
    }) { (error) in
      self.presenter?.didFailLoadData(with: error)
    }
  }
  
  //MARK: - Private
  
  private func prepareData(from quotations: [Double], with identificator: String) -> ChartData {
    let values = (0..<quotations.count).map { (i) -> BarChartDataEntry in
      let quotation = quotations[i]
      return BarChartDataEntry(x: Double(i) + 1, y: quotation)
    }
    
    let set1 = LineChartDataSet(entries: values, label: identificator)
    set1.drawIconsEnabled = false
    set1.setColor(.red)
    set1.setCircleColor(.red)
    set1.lineWidth = 2
    set1.circleRadius = 0
    
    return LineChartData(dataSet: set1)
  }
}
